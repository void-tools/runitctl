mod cli;

use std::io;
use std::process::{ Command, ExitCode };
use clap::Parser;
use fspp::*;

fn main() -> ExitCode {
    let args = cli::Cli::parse();

    match args.command {
        cli::Command::Enable(services) => {
            for i in services.services {
                match enable_service(i) {
                    Ok(_) => (),
                    Err(_) => return ExitCode::FAILURE,
                };
            }
        }
        cli::Command::Disable(services) => {
            for i in services.services {
                match disable_service(i) {
                    Ok(_) => (),
                    Err(_) => return ExitCode::FAILURE,
                };
            }
        }
        cli::Command::List { filter } => {
            match list_services(filter) {
                Ok(o) => {
                    for i in o {
                        println!("[{}] {}", match i.1 {
                            true => "*",
                            false => "-",
                        }, i.0);
                    }
                },
                Err(_) => {
                    eprintln!("Failed to list services!");

                    return ExitCode::FAILURE;
                },
            };
        }
    };

    ExitCode::SUCCESS
}

fn list_services(filter: Option<cli::ListFilter>) -> Result<Vec<(String, bool)>, io::Error> {
    let mut services: Vec<(String, bool)> = Vec::new();

    let enabled: Vec<String> = directory::list_items(&Path::new("/var/service"))?
        .into_iter()
        .map(|x| x.basename())
        .collect();

    for i in directory::list_items(&Path::new("/etc/sv"))?.into_iter().map(|x| x.basename()) {
        services.push((i.clone(), enabled.contains(&i)));
    }

    services = match filter {
        None => services,
        Some(s) => match s {
            cli::ListFilter::Enabled => services.into_iter().filter(|x| x.1 == true).collect(),
            cli::ListFilter::Disabled => services.into_iter().filter(|x| x.1 == false).collect(),
        },
    };

    Ok(services)
}

fn disable_service<S: AsRef<str>>(service: S) -> Result<(), io::Error> {
    let path = Path::new("/var/service").add_str(service.as_ref());

    if path.exists() == false {
        if Path::new("/etc/sv").add_str(service.as_ref()).exists() {
            println!("Service already disabled! ('{}')", service.as_ref());

            return Ok(());
        }

        else {
            eprintln!("Service not found! ('{}')", service.as_ref());

            return Err(io::Error::new(
                io::ErrorKind::NotFound,
                "Service not found!",
            ));
        }
    }

    match command(format!("unlink {}", path.to_string())) {
        true => (),
        false => {
            eprintln!("Failed to disable service: '{}'", service.as_ref());

            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Failed to disable service!",
            ));
        }
    };

    println!("Disabled service: '{}'", service.as_ref());

    Ok(())
}

fn enable_service<S: AsRef<str>>(service: S) -> Result<(), io::Error> {
    let path = Path::new("/etc/sv").add_str(service.as_ref());

    if path.exists() == false {
        eprintln!("Service not found! ('{}')", service.as_ref());

        return Err(io::Error::new(
            io::ErrorKind::NotFound,
            "Service not found!",
        ));
    }

    let added_path = Path::new("/var/service").add_str(service.as_ref());

    if added_path.exists() {
        println!("Service already enabled! ('{}')", service.as_ref());

        return Ok(());
    }

    match command(format!("ln -s {} /var/service", path.to_string())) {
        true => (),
        false => {
            eprintln!("Failed to enable service: '{}'", service.as_ref());

            return Err(io::Error::new(
                io::ErrorKind::Other,
                "Failed to enable service!",
            ));
        }
    };

    println!("Enabled service: '{}'", service.as_ref());

    Ok(())
}

fn command<S: AsRef<str>>(command: S) -> bool {
    match Command::new("bash").args(["-c", command.as_ref()]).status() {
        Ok(o) => o,
        Err(_) => return false,
    }.success()
}
