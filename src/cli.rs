use clap::{ Args, Parser, Subcommand, ValueEnum };

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
/// Cleaner way to enable/disable startup services
pub struct Cli {
    #[clap(subcommand)]
    pub command: Command,
}

#[derive(Subcommand)]
pub enum Command {
    /// Enable startup services
    Enable(Services),
    /// Disable startup services
    Disable(Services),
    /// List all services
    List {
        #[clap(short, long)]
        /// Show services that match the filter option
        filter: Option<ListFilter>,
    },
}

#[derive(Args)]
pub struct Services {
    pub services: Vec<String>,
}

#[derive(ValueEnum, Clone, Copy)]
pub enum ListFilter {
    Enabled,
    Disabled,
}
