# Runitctl

A proper utility for managing startup services on Void Linux and other Runit-based systems.

## Usage:
- Enabling services: `runitctl enable [SERVICES]`
- Disabling services: `runitctl disable [SERVICES]`
- Listing services: `runitctl list`
- Listing enabled services: `runitctl list --filter enabled`
- Listing disabled services: `runitctl list --filter disabled`